package de.sommercom.collider;

import android.content.Context;
import android.opengl.GLSurfaceView;

import de.sommercom.collider.model.GameWorld;

class GameSurfaceView extends GLSurfaceView {
	public GameRenderer renderer;
	
	public GameSurfaceView(Context context, GameWorld world) {
		super(context);

	}
}