package de.sommercom.collider.model;

import org.jbox2d.common.Vec2;

public interface Player extends Entity {

	public abstract float getRadius();

	public abstract Vec2 getPosition();

	public abstract void stopMoving();

	public abstract void startMoveRight();

	public abstract void startMoveLeft();

	public abstract boolean isDead();

	public abstract void setDead(boolean dead);

	public abstract void shoot();

	public abstract boolean isWeaponCoolDown();

	public abstract void setWeaponCoolDown(boolean weaponCoolDown);

	public abstract void setGravity(boolean gravity);

	public abstract void startMoveUp();

	public abstract void startMoveDown();

}