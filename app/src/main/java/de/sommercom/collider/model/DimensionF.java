package de.sommercom.collider.model;

public class DimensionF {
	public final float Width;
	public final float Height;
	
	public DimensionF(float width, float height) {
		this.Width = width;
		this.Height = height;
	}
}
