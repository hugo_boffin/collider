package de.sommercom.collider.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;

public class Ladder implements Entity {
	private final Body body;
	private final PolygonShape polygon;
	private final Vec2 halfSize;
	private final GameWorld world;
	
	public Ladder(GameWorld world, Vec2 centerPos, Vec2 halfSize) {
		this.world = world;
		this.halfSize = new Vec2(halfSize);

		BodyDef def = new BodyDef();
		def.position.set(centerPos);
		body = world.getWorld().createBody(def);
		
		polygon = new PolygonShape();
		polygon.setAsBox(halfSize.x, halfSize.y, new Vec2(0, 0), 0.0f);

		FixtureDef fd = new FixtureDef();
		fd.shape = polygon;
		fd.isSensor = true;
		fd.userData = this;

		body.createFixture(fd);
	}
	
	public final Vec2 getPosition() {
		return body.getPosition();
	}

	public Vec2 getHalfSize() {
		return halfSize;
	}
	
	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void beforeWorldStep(float deltaTime) {
	}

	@Override
	public void afterWorldStep(float deltaTime) {
	}

	@Override
	public void destroyBodies() {
		world.getWorld().destroyBody(body);
	}

}
