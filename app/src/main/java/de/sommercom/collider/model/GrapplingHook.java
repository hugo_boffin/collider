package de.sommercom.collider.model;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

public class GrapplingHook implements Entity {
	private final GameWorld world;
	private final Body body;
	private final PolygonShape shape;
	private final Vec2 halfSize;

	public GrapplingHook(GameWorld world, float x, float y) {
		this.world = world;
		halfSize = new Vec2(0.5f, 15.0f);
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position.set(x,y-halfSize.y);
		bodyDef.allowSleep = false;
		bodyDef.linearDamping = 0.0f;
		bodyDef.bullet = true;
		bodyDef.fixedRotation = true;
		
		shape = new PolygonShape();
		shape.setAsBox(halfSize.x, halfSize.y, new Vec2(0,0), 0.0f);

		FixtureDef fd = new FixtureDef();
		fd.userData = this;
		fd.shape = shape;
		fd.friction = 1.0f;
		fd.density = 1.0f;
		fd.restitution = 0.0f;
		fd.filter.categoryBits = GameWorld.C_GROUP_WEAPON;
		fd.filter.maskBits = GameWorld.C_MASK_WEAPON;
				
		body = world.getWorld().createBody(bodyDef);
		body.createFixture(fd);
		body.setLinearVelocity(new Vec2(0,25.0f));
	}
	
	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void beforeWorldStep(float deltaTime) {
	}
	
	@Override
	public void afterWorldStep(float deltaTime) {
	}

	public final Vec2 getPosition() {
		return body.getPosition();
	}

	public Vec2 getHalfSize() {
		return halfSize;
	}

	@Override
	public void destroyBodies() {
		world.getWorld().destroyBody(body);
	}
	
	public void destroy() {
		world.remove(this);
		world.getPlayer().setWeaponCoolDown(false);
	}

}
