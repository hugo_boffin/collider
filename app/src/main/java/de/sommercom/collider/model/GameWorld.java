package de.sommercom.collider.model;

import java.util.ArrayList;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.callbacks.DebugDraw;
import org.jbox2d.collision.Manifold;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.contacts.Contact;

public class GameWorld implements ContactListener {
	public static interface Event {
		void fire();
	}

	public static final int C_GROUP_WALL = 0x0001;
	public static final int C_GROUP_BALL = 0x0002;
	public static final int C_GROUP_PLAYER = 0x0004;
	public static final int C_GROUP_WEAPON = 0x0008;

	public static final int C_MASK_ALL = 0xFFFF;
	public static final int C_MASK_WALL = C_MASK_ALL;
	public static final int C_MASK_BALL = C_MASK_ALL ^ C_GROUP_BALL;
	public static final int C_MASK_PLAYER = C_MASK_ALL ^ C_GROUP_PLAYER;
	public static final int C_MASK_WEAPON = C_MASK_ALL ^ C_GROUP_PLAYER;

	public static final float TIME_FACTOR = 0.75f;
	public static final float UPDATE_RATE = 1.0f / 30.0f;
	public static final int positionIterations = 3;
	public static final int velocityIterations = 8;
	private final World world;
	private Player player;
	private final DimensionF viewSize;
	private final ArrayList<Entity> entities;
	private boolean paused;

	private Event onPlayerDied;

	public GameWorld(DimensionF _viewport) {
		this.entities = new ArrayList<Entity>();
		this.viewSize = _viewport;

		// final Vec2 gravity = new Vec2(0, -9.81f);
		final Vec2 gravity = new Vec2(0, 0);
		world = new World(gravity, false);
		world.setContactListener(this);

		createWorldBody();
		
		paused = false;
	}

	private void createWorldBody() {
		BodyDef bd = new BodyDef();
		Body ground = world.createBody(bd);

		PolygonShape shape = new PolygonShape();

		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		// fd.userData = null;
		fd.friction = 0.0f;
		fd.density = 0.0f;
		fd.filter.categoryBits = GameWorld.C_GROUP_WALL;
		fd.filter.maskBits = GameWorld.C_MASK_WALL;

		shape.setAsEdge(new Vec2(viewSize.Width, viewSize.Height), new Vec2(0,
				viewSize.Height));
		ground.createFixture(fd);

		shape.setAsEdge(new Vec2(0, viewSize.Height), new Vec2(0, 0));
		ground.createFixture(fd);

		shape.setAsEdge(new Vec2(viewSize.Width, 0), new Vec2(viewSize.Width,
				viewSize.Height));
		ground.createFixture(fd);

		fd.userData = this;// TODO Finsterster Hack ever...
		shape.setAsEdge(new Vec2(0, 0), new Vec2(viewSize.Width, 0));
		ground.createFixture(fd);
	}

	public synchronized void update() {
		if (paused)
			return;

		Contact contact = world.getContactList();
		while (contact != null) {
			final Object bodyA = contact.m_fixtureA.getUserData();
			final Object bodyB = contact.m_fixtureB.getUserData();

			if (!handleContacts(bodyA, bodyB)) {
				handleContacts(bodyB, bodyA);
			}

			contact = contact.getNext();
		}

		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).beforeWorldStep(UPDATE_RATE * TIME_FACTOR);
		}

		world.step(UPDATE_RATE * TIME_FACTOR, velocityIterations, positionIterations);

		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).afterWorldStep(UPDATE_RATE * TIME_FACTOR);
		}
	}

	private boolean handleContacts(final Object bodyA, final Object bodyB) {
		if (bodyA instanceof ConcretePlayer && bodyB instanceof Ball) {
			Player player = ((Player) bodyA);

			if (!player.isDead()) {
				player.setDead(true);
				if (onPlayerDied != null)
					onPlayerDied.fire();
			}

			return true;
		}
		if (bodyA instanceof Ball && bodyB instanceof GrapplingHook) {
			((Ball) bodyA).divide();
			((GrapplingHook) bodyB).destroy();
			return true;
		}
		if (!(bodyA instanceof GameWorld) && bodyB instanceof GrapplingHook) {
			((GrapplingHook) bodyB).destroy();
			return true;
		}

		return false;
	}

	public boolean add(Entity entity) {
		if (entities.contains(entity))
			return false;

		entities.add(entity);
		return true;
	}

	public void clearAll() {
		while (!entities.isEmpty()) {
			remove(entities.get(0));
		}
		// entities.clear();
	}

	public boolean remove(Entity entity) {
		if (entities.contains(entity)) {
			entities.remove(entity);
			entity.destroyBodies();
			return true;
		}
		return false;
	}

	public Entity get(int index) {
		return entities.get(index);
	}

	public int count() {
		return entities.size();
	}

	public void setDebugDraw(DebugDraw debugDraw) {
		world.setDebugDraw(debugDraw);
	}

	public void drawDebugData() {
		world.drawDebugData();
	}

	public World getWorld() {
		return world;
	}

	public DimensionF getViewSize() {
		return viewSize;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
	public void beginContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		
		if (fixtureA.getUserData() instanceof Ladder && fixtureB.getUserData() instanceof ConcretePlayer) {
			((Player)fixtureB.getUserData()).setGravity(false);
		}

		if (fixtureB.getUserData() instanceof Ladder && fixtureA.getUserData() instanceof ConcretePlayer) {
			((Player)fixtureA.getUserData()).setGravity(false);
		}
	}

	@Override
	public void endContact(Contact contact) {
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		
		if (fixtureA.getUserData() instanceof Ladder && fixtureB.getUserData() instanceof ConcretePlayer) {
			((Player)fixtureB.getUserData()).setGravity(true);
		}

		if (fixtureB.getUserData() instanceof Ladder && fixtureA.getUserData() instanceof ConcretePlayer) {
			((Player)fixtureA.getUserData()).setGravity(true);
		}
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		final Object bodyA = contact.m_fixtureA.getUserData();
		final Object bodyB = contact.m_fixtureB.getUserData();

		if (bodyA instanceof GameWorld && bodyB instanceof GrapplingHook)
			contact.setEnabled(false);
		if (bodyB instanceof GameWorld && bodyA instanceof GrapplingHook)
			contact.setEnabled(false);

		if (bodyA instanceof ConcretePlayer && bodyB instanceof Ball)
			contact.setEnabled(false);
		if (bodyB instanceof ConcretePlayer && bodyA instanceof Ball)
			contact.setEnabled(false);

		if (bodyA instanceof Ball && bodyB instanceof GrapplingHook) {
			contact.setEnabled(false);
		}
		if (bodyB instanceof Ball && bodyA instanceof GrapplingHook) {
			contact.setEnabled(false);
		}
	}

	public void setOnPlayerDied(Event onPlayerDied) {
		this.onPlayerDied = onPlayerDied;
	}

	public boolean isPaused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}
}
