package de.sommercom.collider.model;

import org.jbox2d.dynamics.Body;

public interface Entity {
	Body getBody();
	void beforeWorldStep(float deltaTime);
	void afterWorldStep(float deltaTime);
	void destroyBodies();
}