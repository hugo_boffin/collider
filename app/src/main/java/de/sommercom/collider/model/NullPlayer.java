package de.sommercom.collider.model;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class NullPlayer implements Player {

	@Override
	public Body getBody() {
		return null;
	}

	@Override
	public void beforeWorldStep(float deltaTime) {
	}

	@Override
	public void afterWorldStep(float deltaTime) {
	}

	@Override
	public void destroyBodies() {
	}

	@Override
	public float getRadius() {
		return 0;
	}

	@Override
	public Vec2 getPosition() {
		return null;
	}

	@Override
	public void stopMoving() {
	}

	@Override
	public void startMoveRight() {
	}

	@Override
	public void startMoveLeft() {
	}

	@Override
	public boolean isDead() {
		return false;
	}

	@Override
	public void setDead(boolean dead) {
	}

	@Override
	public void shoot() {
	}

	@Override
	public boolean isWeaponCoolDown() {
		return false;
	}

	@Override
	public void setWeaponCoolDown(boolean weaponCoolDown) {
	}

	@Override
	public void setGravity(boolean gravity) {
	}

	@Override
	public void startMoveUp() {
	}

	@Override
	public void startMoveDown() {
	}

}
