package de.sommercom.collider.model;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

public class Ball implements Entity {
	private final GameWorld world;
	private final Body body;
	private final CircleShape circle;
	private final int divisions;
	private final float radius;
	
	public Ball(GameWorld world, float x, float y, float radius, float linX, float linY, int divisions) {
		this.radius = radius;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.linearVelocity.x = linX;
		bodyDef.linearVelocity.y = linY;
		bodyDef.linearDamping = 0.0f;
		bodyDef.angularDamping = 0.0f;
		bodyDef.position.set(x, y);
		
		circle = new CircleShape();
		circle.m_radius = radius;
		
		FixtureDef fd = new FixtureDef();
		fd.shape = circle;
		fd.friction = 0.0f;
		fd.density = 1.0f;
		fd.restitution = 0.99f;
		fd.userData = this;
		fd.filter.categoryBits = GameWorld.C_GROUP_BALL;
		fd.filter.maskBits = GameWorld.C_MASK_BALL;
		
		body = world.getWorld().createBody(bodyDef);
		body.createFixture(fd);
		
		this.divisions = divisions;
		this.world = world;
	}

	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void beforeWorldStep(float deltaTime) {
		body.applyForce(new Vec2(0,/*0.1f**/-9.81f*body.getMass()), body.getWorldCenter());
	}
	
	public float getRadius() {
		return circle.m_radius;
	}
	
	public final Vec2 getPosition() {
		return body.getPosition();
	}
	
	public int getDivisions() {
		return divisions;
	}

	@Override
	public void afterWorldStep(float deltaTime) {		
	}

	public void divide() {
		final float velocity = body.getLinearVelocity().length();
		
		world.remove(this);
		
		if (divisions>0) {
			Vec2 pos = body.getPosition();
			world.add(new Ball(world,pos.x-radius*0.25f, pos.y,radius*0.5f, -velocity*0.75f, 0.0f, divisions-1));
			world.add(new Ball(world,pos.x+radius*0.25f, pos.y,radius*0.5f, +velocity*0.75f, 0.0f, divisions-1));
		}
	}

	@Override
	public void destroyBodies() {
		world.getWorld().destroyBody(body);
	}
}
