package de.sommercom.collider.model;

public class Position {
	public static final Position ZERO = new Position(0, 0);
	
	public final int x;
	public final int y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
