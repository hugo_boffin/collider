package de.sommercom.collider.model;

public class Dimension {
	public final int Width;
	public final int Height;
	
	public Dimension(int width, int height) {
		this.Width = width;
		this.Height = height;
	}
}
