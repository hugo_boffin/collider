package de.sommercom.collider.model;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import android.util.Log;

public class ConcretePlayer implements Player {
	private final Body body;
	private final CircleShape circle;
	private final Vec2 velocity;
	private final GameWorld world;
//	private int hitPoints;
	private boolean dead;
	private boolean weaponCoolDown;
	private boolean gravity;

	public ConcretePlayer(GameWorld world, float x, float y, float radius) {
		this.world = world;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.linearDamping = 0.0f;
		bodyDef.angularDamping = 0.0f;
		bodyDef.position.set(x, y);
		bodyDef.fixedRotation = true;
		bodyDef.allowSleep = false;

		circle = new CircleShape();
		circle.m_radius = radius;

		FixtureDef fd = new FixtureDef();
		fd.shape = circle;
		fd.friction = 1.0f;
		fd.density = 150.0f;
		fd.restitution = 0.0f;
		fd.filter.categoryBits = GameWorld.C_GROUP_PLAYER;
		fd.filter.maskBits = GameWorld.C_MASK_PLAYER;
		fd.userData = this;

		gravity = true;
		body = world.getWorld().createBody(bodyDef);
		body.createFixture(fd);

		velocity = new Vec2();

		dead = false;
		weaponCoolDown = false;
	}

	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void beforeWorldStep(float deltaTime) {
		if (gravity) {
			body.applyForce(new Vec2(0,/*0.1f**/-9.81f*body.getMass()), body.getWorldCenter());
		}
	}

	@Override
	public void afterWorldStep(float deltaTime) {
	}

	@Override
	public void destroyBodies() {
		world.getWorld().destroyBody(body);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#getRadius()
	 */
	@Override
	public float getRadius() {
		return circle.m_radius;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#getPosition()
	 */
	@Override
	public final Vec2 getPosition() {
		return body.getPosition();
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#stopMoving()
	 */
	@Override
	public void stopMoving() {
		velocity.set(0.0f, 0.0f);
		body.setLinearVelocity(velocity);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#startMoveRight()
	 */
	@Override
	public void startMoveRight() {
		velocity.x = 15.0f;
		body.setLinearVelocity(velocity);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#startMoveLeft()
	 */
	@Override
	public void startMoveLeft() {
		velocity.x = -15.0f;
		body.setLinearVelocity(velocity);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#isDead()
	 */
	@Override
	public boolean isDead() {
		return dead;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#setDead(boolean)
	 */
	@Override
	public void setDead(boolean dead) {
		this.dead = dead;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#shoot()
	 */
	@Override
	public void shoot() {
		if (!weaponCoolDown) {
			weaponCoolDown = true;
			final Vec2 pos = body.getPosition();
			world.add(new GrapplingHook(world, pos.x, pos.y - circle.m_radius));
		}
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#isWeaponCoolDown()
	 */
	@Override
	public boolean isWeaponCoolDown() {
		return weaponCoolDown;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#setWeaponCoolDown(boolean)
	 */
	@Override
	public void setWeaponCoolDown(boolean weaponCoolDown) {
		this.weaponCoolDown = weaponCoolDown;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#setGravity(boolean)
	 */
	@Override
	public void setGravity(boolean gravity) {
		this.gravity = gravity;
		if (gravity) {
			Vec2 velo = body.getLinearVelocity();
			velo.y = 0.0f;
			body.setLinearVelocity(velo);	
		}
		Log.d("PLAYER", "Gravity: "+gravity);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#startMoveUp()
	 */
	@Override
	public void startMoveUp() {
		if (!gravity) {
			velocity.x = 0.0f;
			velocity.y = 15.0f;
			body.setLinearVelocity(velocity);		
		}
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.model.Player#startMoveDown()
	 */
	@Override
	public void startMoveDown() {
		if (!gravity) {
			velocity.x = 0.0f;
			velocity.y = -15.0f;
			body.setLinearVelocity(velocity);		
		}
	}
	
}
