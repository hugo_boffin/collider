package de.sommercom.collider;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

import de.sommercom.collider.model.Dimension;

public class BasicTexture implements Texture {
	private final int Handle;
	private final Dimension Size;
	private final int[] fullCrop;
	
	public BasicTexture(Context context, GL10 gl, int id) {
    	final int[] textures = new int[1];
    	final Dimension bitmapSize;
    	
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glGenTextures(1, textures, 0);

        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
        
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

        //gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);
        
        final InputStream is = context.getResources().openRawResource(id);
        final Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(is);
        } finally {
            try {
                is.close();
            } catch(IOException e) {
            	e.printStackTrace();
            }
        }

        bitmapSize = new Dimension(bitmap.getWidth(),bitmap.getHeight());
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
       
        bitmap.recycle();
        
        Handle = textures[0];
        Size = bitmapSize;
        fullCrop = new int[] {0, bitmapSize.Height, bitmapSize.Width, -bitmapSize.Height};
        
//		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
//				GL11Ext.GL_TEXTURE_CROP_RECT_OES, fullCrop, 0);
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.Texture#getHandle()
	 */
	@Override
	public int getHandle() {
		return Handle;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.Texture#getSize()
	 */
	@Override
	public Dimension getSize() {
		return Size;
	}

	/* (non-Javadoc)
	 * @see de.sommercom.collider.Texture#getFullCrop()
	 */
	@Override
	public int[] getCrop() {
		return fullCrop;
	}
}
