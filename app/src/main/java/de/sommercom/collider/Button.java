package de.sommercom.collider;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.graphics.Rect;

import de.sommercom.collider.model.Dimension;
import de.sommercom.collider.model.Position;

public class Button {
	private final Position position;
	private final Dimension size;
	private final Rect bounds;
	private final Texture mouseUp;
	private final Texture mouseDown;
	private boolean down = false;
	
	public static interface Event {
		void fire();
	}
	
	private Event onMouseDown;
	private Event onMouseUp;
	
	public Button(Position position, Dimension size, Texture mouseUp,
			Texture mouseDown) {
		this.position = position;
		this.size = size;
		this.mouseUp = mouseUp;
		this.mouseDown = mouseDown;
		this.bounds = new Rect(position.x,position.y,position.x+size.Width,position.y+size.Height);
	}

	public void draw(GL10 gl) {
		final Texture active = down ? mouseDown : mouseUp;
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, active.getHandle());
		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
				GL11Ext.GL_TEXTURE_CROP_RECT_OES, active.getCrop(), 0);
		((GL11Ext)gl).glDrawTexfOES(position.x, position.y, 0, size.Width, size.Height);
	}
	
	public Position getPosition() {
		return position;
	}

	public Dimension getSize() {
		return size;
	}
		
	public Rect getBounds() {
		return bounds;
	}

	public boolean isDown() {
		return down;
	}

	public void mouseDown(int x, int y) {
		if (onMouseDown != null)
			onMouseDown.fire();
		down = true;
	}
	
	public void mouseUp() {
		if (onMouseUp != null)
			onMouseUp.fire();
		down = false;
	}

	public void setOnMouseDown(Event onMouseDown) {
		this.onMouseDown = onMouseDown;
	}

	public void setOnMouseUp(Event onMouseUp) {
		this.onMouseUp = onMouseUp;
	}
}
