package de.sommercom.collider;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import com.google.ads.AdRequest;
import com.google.ads.AdView;
import de.sommercom.collider.model.DimensionF;
import de.sommercom.collider.model.GameWorld;
import de.sommercom.collider.model.Player;

public class GameActivity extends Activity {
	private static final int DIALOG_GAMEOVER_ID = 0;

	private boolean musicEnabled;
	private boolean adsEnabled;
	// Controller -> shoot();
	private GLSurfaceView glSurfaceView;
	private GameRenderer renderer;
	private WakeLock wakeLock;
	private Player player;
	private MediaPlayer mediaPlayer;
	private DimensionF abstractViewport;
	private GameWorld world;
	private int level;
	private Thread gameThread;
	private GameLogicRunnable gameRunnable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Load Screen
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		musicEnabled = prefs.getBoolean(
				getString(R.string.preferences_audio_musicEnabled), true);
		adsEnabled = prefs.getBoolean(
				getString(R.string.preferences_debug_adsEnabled), true);
		final RendererOptions rendererOptions = new RendererOptions(
				prefs.getBoolean(
						getString(R.string.preferences_video_freakMode), true));

		if (musicEnabled) {
            //TODO: Removed music for the moment.
//			mediaPlayer = MediaPlayer.create(this, R.raw.music01);
//			mediaPlayer.start();
		}

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);

		setContentView(R.layout.game);

		final PowerManager powerManager = (PowerManager) this
				.getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
				"Game Lock");

		if (adsEnabled) {
			AdRequest request = new AdRequest();
			//request.addTestDevice(AdRequest.TEST_EMULATOR);
			AdView adView = (AdView) this.findViewById(R.id.adView);
			adView.loadAd(request);
			adView.setVisibility(AdView.VISIBLE);
		}
		level = 0;

		Bundle extras = getIntent().getExtras();
		if (extras != null && extras.containsKey("Level")) {
			level = extras.getInt("Level");
		}

		Display display = getWindowManager().getDefaultDisplay(); 
		//abstractViewport = new Dimension(50, 30);
		abstractViewport = new DimensionF(50, (float)display.getHeight()/(float)display.getWidth() * 50);
		world = new GameWorld(abstractViewport);

		Timer timer = new Timer(true);

		renderer = new GameRenderer(this, world, timer, rendererOptions);

		glSurfaceView = (GLSurfaceView) this.findViewById(R.id.glSurface);
		glSurfaceView.setEGLConfigChooser(8, 8, 8, 0, 0, 0);
		glSurfaceView.setEGLConfigChooser(false);
		glSurfaceView.setRenderer(renderer);

		world.setOnPlayerDied(new GameWorld.Event() {
			@Override
			public void fire() {
				GameActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						world.setPaused(true);
						showDialog(DIALOG_GAMEOVER_ID);
					}
				});
			}
		});

		gameRunnable = new GameLogicRunnable(world, timer);
		gameThread = new Thread(gameRunnable);
		gameThread.start();

		synchronized (world) {
			startLevel();
			world.setPaused(false);
		}
	}

	private void startLevel() {
		new LevelFactory().loadLevel(world, abstractViewport, level);
		player = world.getPlayer();
	}

	@Override
	protected void onPause() {
		if (mediaPlayer != null) {
			mediaPlayer.pause();
		}
		// TODO: Bei zuviel Speicherverbrauch: Freigabe von Texturen, etc.
		super.onPause();
		wakeLock.release();
		glSurfaceView.onPause();
	}

	@Override
	protected void onResume() {
		if (mediaPlayer != null) {
			mediaPlayer.start();
		}
		// TODO: Bei zuviel Speicherverbrauch: Reallokation von Texturen, etc.
		super.onResume();
		wakeLock.acquire();
		glSurfaceView.onResume();
	}

	@Override
	protected void onDestroy() {
		gameRunnable.setRunning(false);
		try {
			gameThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.d("DEBUG","Joined Thread gracefully.");
		super.onDestroy();
	}

	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			glSurfaceView.queueEvent(new Runnable() {
				@Override
				public void run() {
					renderer.touchDown(
							(int) event.getX(),
							renderer.getFullViewSize().Height
									- (int) event.getY());
				}
			});
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_CANCEL:
			// TODO Like Up, but no action
			break;
		case MotionEvent.ACTION_UP:
			glSurfaceView.queueEvent(new Runnable() {
				@Override
				public void run() {
					renderer.touchUp();
				}
			});
			break;
		default:
			return super.onTouchEvent(event);
		}

		try {
			Thread.sleep(16);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			synchronized(player) {
				player.startMoveLeft();
			}
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			synchronized(player) {
				player.startMoveRight();
			}
			break;
		case KeyEvent.KEYCODE_SPACE:
			synchronized(player) {
				player.shoot();
			}
			break;
		default:
			return super.onKeyDown(keyCode, event);
		}
		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			synchronized(player) {
				player.stopMoving();
			}
			break;
		case KeyEvent.KEYCODE_SPACE:
			// Stop shooting?
			break;
		default:
			return super.onKeyUp(keyCode, event);
		}
		return true;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog dialog;
		switch (id) {
		case DIALOG_GAMEOVER_ID:
			final CharSequence[] items = { "Retry", "Main Menu" };

			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setCancelable(false);
			builder.setTitle("You are dead :-(");
			builder.setItems(items, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					Log.d("Dialog", "Item selected " + item);
					switch (item) {
					case 0:
						synchronized (world) {
							startLevel();
							world.setPaused(false);
						}
						break;
					default:
						Intent intent = new Intent(getBaseContext(),
								MainMenuActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
					}
				}
			});
			dialog = builder.create();
			break;
		default:
			dialog = null;
		}
		return dialog;
	}

}
