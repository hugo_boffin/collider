package de.sommercom.collider;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import org.jbox2d.common.Vec2;

import android.content.Context;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.util.Log;

import de.sommercom.collider.model.Ball;
import de.sommercom.collider.model.ConcretePlayer;
import de.sommercom.collider.model.Dimension;
import de.sommercom.collider.model.DimensionF;
import de.sommercom.collider.model.Entity;
import de.sommercom.collider.model.GameWorld;
import de.sommercom.collider.model.GrapplingHook;
import de.sommercom.collider.model.Ladder;
import de.sommercom.collider.model.Platform;
import de.sommercom.collider.model.Player;
import de.sommercom.collider.model.Position;

public class GameRenderer implements GLSurfaceView.Renderer, Observer {
	public static final float UPDATE_RATE = 1.0f / 60.0f;	
	private final RendererOptions options;
	private final Context context;
	private final GameWorld world;
	private final Timer timer;
	private final StringBuilder fpsBuilder = new StringBuilder(32);
	private final Random rand = new Random();
	private final ArrayList<Button> buttons;
	private Button active = null;
	private Texture playerImage;
	private Texture ballImage;
	private Texture backImage;
	private Texture platImage;
	private Texture hookImage;
	private Dimension fullViewSize;
	private Dimension gameViewSize;
	private DimensionF abstractViewSize;
	private float scaleX;
	private float scaleY;
	// private PhysicsDebugDraw debugDraw;
	private boolean useAllPixels;

	public GameRenderer(Context context, GameWorld world, Timer timer,
			RendererOptions rendererOptions) {
		this.context = context;
		this.options = rendererOptions;
		this.timer = timer;
		 this.timer.addObserver(this);
		this.fpsBuilder.append("FPS: ");
		this.useAllPixels = true;
		this.buttons = new ArrayList<Button>();
		this.world = world;
		this.abstractViewSize = world.getViewSize();
		// debugDraw = new PhysicsDebugDraw();
		// world.setDebugDraw(debugDraw);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		gl.glClearColor(54.0f / 255.0f, 188.0f / 255.0f, 238.0f / 255.0f,
				255.0f);
		gl.glDisable(GL10.GL_DITHER);
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);

		playerImage = new BasicTexture(context, gl, R.raw.robot);
		ballImage = new BasicTexture(context, gl, R.raw.ball);
		backImage = new BasicTexture(context, gl, R.raw.wallpaper);
		platImage = new BasicTexture(context, gl, R.raw.platform);
		hookImage = new BasicTexture(context, gl, R.raw.hook);

		gl.glEnable(GL10.GL_TEXTURE_2D);
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		final GL11Ext gl11Ext = (GL11Ext) gl;

		timer.step();
		
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		gl.glDisable(GL10.GL_BLEND);

		if (options.freakColors) {
			gl.glColor4f(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(),
					1.0f);
		} else {
			gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		}

		gl.glBindTexture(GL10.GL_TEXTURE_2D, backImage.getHandle());
		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
				GL11Ext.GL_TEXTURE_CROP_RECT_OES, backImage.getCrop(), 0);
		gl11Ext.glDrawTexiOES(0, 0, 0, gameViewSize.Width, gameViewSize.Height);

		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		synchronized (world) {
			gl.glBindTexture(GL10.GL_TEXTURE_2D, platImage.getHandle());
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, platImage.getCrop(), 0);
			for (int i = 0; i < world.count(); i++) {
				final Entity entity = world.get(i);
	
				if (entity instanceof Platform) {
					final Platform platform = (Platform) entity;
					final Vec2 pos = platform.getPosition();
					final Vec2 halfSize = platform.getHalfSize();
	
					if (options.freakColors) {
						gl.glColor4f(rand.nextFloat(), rand.nextFloat(),
								rand.nextFloat(), 1.0f);
					}
					drawTexture(gl11Ext, pos.x - halfSize.x, pos.y - halfSize.y,
							2.0f * halfSize.x, 2.0f * halfSize.y);
				}
			}
			
			gl.glBindTexture(GL10.GL_TEXTURE_2D, platImage.getHandle());
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, platImage.getCrop(), 0);
			gl.glColor4f(1.0f, 1.0f, 1.0f, 0.75f);
			for (int i = 0; i < world.count(); i++) {
				final Entity entity = world.get(i);

				if (entity instanceof Ladder) {
					final Ladder ladder = (Ladder) entity;
					final Vec2 pos = ladder.getPosition();
					final Vec2 halfSize = ladder.getHalfSize();
	
					if (options.freakColors) {
						gl.glColor4f(rand.nextFloat(), rand.nextFloat(),
								rand.nextFloat(), 0.5f);
					}
					drawTexture(gl11Ext, pos.x - halfSize.x, pos.y - halfSize.y,
							2.0f * halfSize.x, 2.0f * halfSize.y);
				}
			}
			gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

			gl.glBindTexture(GL10.GL_TEXTURE_2D, hookImage.getHandle());
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, hookImage.getCrop(), 0);
			for (int i = 0; i < world.count(); i++) {
				final Entity entity = world.get(i);
	
				if (entity instanceof GrapplingHook) {
					final GrapplingHook hook = (GrapplingHook) entity;
					final Vec2 pos = hook.getPosition();
//					final Vec2 halfSize = hook.getHalfSize();
	
					if (options.freakColors) {
						gl.glColor4f(rand.nextFloat(), rand.nextFloat(),
								rand.nextFloat(), 1.0f);
					}
					drawTexture(gl11Ext, pos.x - 2f, pos.y - 16f,
							2.0f * 2f, 2.0f * 16f);
				}
			}
	
			gl.glBindTexture(GL10.GL_TEXTURE_2D, playerImage.getHandle());
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, playerImage.getCrop(), 0);
			for (int i = 0; i < world.count(); i++) {
				final Entity entity = world.get(i);
	
				if (entity instanceof ConcretePlayer) {
					final Player player = (Player) entity;
					final Vec2 pos = player.getPosition();
					final float radius = player.getRadius();
					final float texS = radius * 2.0f;
	
					if (options.freakColors) {
						gl.glColor4f(rand.nextFloat(), rand.nextFloat(),
								rand.nextFloat(), 1.0f);
					}
					drawTexture(gl11Ext, pos.x - radius, pos.y - radius, texS, texS);
				}
			}
	
			gl.glBindTexture(GL10.GL_TEXTURE_2D, ballImage.getHandle());
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D,
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, ballImage.getCrop(), 0);
			for (int i = 0; i < world.count(); i++) {
				final Entity entity = world.get(i);
	
				if (entity instanceof Ball) {
					final Ball ball = (Ball) entity;
					final Vec2 pos = ball.getPosition();
					final float radius = ball.getRadius();
					final float texS = radius * 2.0f;
	
					if (options.freakColors) {
						gl.glColor4f(rand.nextFloat(), rand.nextFloat(),
								rand.nextFloat(), 1.0f);
					}
					drawTexture(gl11Ext, pos.x - radius, pos.y - radius, texS, texS);
				}
			}

		}
		// world.drawDebugData();

		// XXX Game: Scaled
		// XXX UI: pixel-accurate
		gl.glColor4f(1f,1f,1f,0.75f);
		for (int i = 0; i < buttons.size(); i++) {
			buttons.get(i).draw(gl);
		}
		
		timer.stepNoFPS();
		
		final float drawingTime = timer.getDeltaTime();
		if (drawingTime < UPDATE_RATE) {
			try {
				Thread.sleep((int) (1000.0f * (UPDATE_RATE - drawingTime)));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void drawTexture(GL11Ext gl, float x, float y, float w, float h) {
		gl.glDrawTexfOES(x * scaleX, y * scaleX, 0, w * scaleX, h * scaleY);
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		gl.glViewport(0, 0, width, height);
		fullViewSize = new Dimension(width, height);

		if (useAllPixels) {
			scaleX = width / abstractViewSize.Width;
			scaleY = height / abstractViewSize.Height;
			gameViewSize = fullViewSize;
		} else {
			// XXX scale based upon width / height ratio, dead pixels allowed
			// XXX Schmalere Seite benutzen, evtl. tote Pixel an einer Kante
		}
		
		setupUI(gl);

		timer.reset();
	}

	private void setupUI(GL10 gl) {
		final BasicTexture uiTexture = new BasicTexture(context, gl, R.raw.ui);

		final Button.Event stopMoving = new Button.Event() {
			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().stopMoving();
				}
			}
		};

		final int tileSize = fullViewSize.Height < 480 ? 32 : 64;
		final Dimension size = new Dimension(tileSize, tileSize);
		final Dimension sizeTex = new Dimension(64, 64);
		final Dimension sizeD = new Dimension(128, 64);
		buttons.clear();

		final Button moveLeft = new Button(new Position(0, tileSize), size,
				new PartialTexture(uiTexture, new Position(0, 0), sizeTex),
				new PartialTexture(uiTexture, new Position(0, 64), sizeTex));
		moveLeft.setOnMouseDown(new Button.Event() {
			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().startMoveLeft();
				}
			}
		});
		moveLeft.setOnMouseUp(stopMoving);
		buttons.add(moveLeft);

		final Button moveRight = new Button(new Position(2*tileSize, tileSize), size,
				new PartialTexture(uiTexture, new Position(64, 0), sizeTex),
				new PartialTexture(uiTexture, new Position(64, 64), sizeTex));
		moveRight.setOnMouseDown(new Button.Event() {
			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().startMoveRight();
				}
			}
		});
		moveRight.setOnMouseUp(stopMoving);
		buttons.add(moveRight);

		final Button moveUp = new Button(new Position(tileSize, 2*tileSize), size, new PartialTexture(
				uiTexture, new Position(3*64, 0), sizeTex), new PartialTexture(
				uiTexture, new Position(3*64, 64), sizeTex));
		moveUp.setOnMouseDown(new Button.Event() {
			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().startMoveUp();
				}
			}
		});
		moveUp.setOnMouseUp(stopMoving);
		buttons.add(moveUp);

		final Button moveDown = new Button(new Position(tileSize, 0), size, new PartialTexture(
				uiTexture, new Position(2*64, 0), sizeTex), new PartialTexture(
				uiTexture, new Position(2*64, 64), sizeTex));
		moveDown.setOnMouseDown(new Button.Event() {
			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().startMoveDown();
				}
			}
		});
		moveDown.setOnMouseUp(stopMoving);
		buttons.add(moveDown);

		final Button shot = new Button(new Position(fullViewSize.Width - 128, 0), sizeD,
				new PartialTexture(uiTexture, new Position(0, 256 - 64),	sizeD), 
				new PartialTexture(uiTexture, new Position(128,	256 - 64), 	sizeD));
		shot.setOnMouseDown(new Button.Event() {

			@Override
			public void fire() {
				synchronized (world) {
					world.getPlayer().shoot();					
				}
			}
		});
		buttons.add(shot);
	}

	@Override
	public void update(Observable sender, Object data) {
		if (sender instanceof Timer) {
			fpsBuilder.setLength(5);
			fpsBuilder.append(((Timer) sender).getFrameRate());
			Log.d("TIME-RENDERER", fpsBuilder.toString());
		}
	}

	public void touchDown(int x, int y) {
		if (active != null)
			return;

		for (int i = 0; i < buttons.size(); i++) {
			final Button button = buttons.get(i);
			final Rect bounds = button.getBounds();
			if (bounds.contains(x, y)) {
				active = button;
				final Position position = active.getPosition();
				active.mouseDown(x - position.x, y - position.y);
			}
		}
	}

	public void touchUp() {
		if (active != null) {
			active.mouseUp();
			active = null;
		}
	}

	public Dimension getFullViewSize() {
		return fullViewSize;
	}
}