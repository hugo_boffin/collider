package de.sommercom.collider;

import java.util.Observable;
import java.util.Observer;

import android.util.Log;

import de.sommercom.collider.model.GameWorld;

public class GameLogicRunnable implements Runnable, Observer {
	private boolean running = true;
	private final GameWorld world;
	private final Timer timer;
	private final StringBuilder fpsBuilder = new StringBuilder(32);

	public GameLogicRunnable(GameWorld world, Timer timer2) {
		this.world = world;
		this.timer = new Timer(true);
		this.fpsBuilder.append("FPS: ");

		timer.addObserver(this);
	}

	public void run() {
		Log.d("DEBUG", "START");
		// TODO pause hier rein?
		timer.reset();
		float timeElapsed = 0;
		while (running) {
			timer.step();
			timeElapsed += timer.getDeltaTime();

			while (timeElapsed < GameWorld.UPDATE_RATE) {
				try {
					Thread.sleep((int) (1000.0f * (GameWorld.UPDATE_RATE - timeElapsed)));
				} catch (InterruptedException e) {
				}

				timer.stepNoFPS();
				timeElapsed += timer.getDeltaTime();
			}

//			synchronized (world) {
				while (timeElapsed >= GameWorld.UPDATE_RATE) {
					world.update();
					timeElapsed -= GameWorld.UPDATE_RATE;
				}
//			}
		}
		Log.d("DEBUG", "END");
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	public void update(Observable sender, Object data) {
		if (sender instanceof Timer) {
			fpsBuilder.setLength(5);
			fpsBuilder.append(((Timer) sender).getFrameRate());
			Log.d("TIME-LOGIC", fpsBuilder.toString());
		}
	}
}
