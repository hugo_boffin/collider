package de.sommercom.collider;

import de.sommercom.collider.model.Dimension;

public interface Texture {

	public abstract int getHandle();

	public abstract Dimension getSize();

	public abstract int[] getCrop();

}