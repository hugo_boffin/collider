package de.sommercom.collider;

import java.util.Random;

import org.jbox2d.common.Vec2;

import de.sommercom.collider.model.Ball;
import de.sommercom.collider.model.ConcretePlayer;
import de.sommercom.collider.model.DimensionF;
import de.sommercom.collider.model.GameWorld;
import de.sommercom.collider.model.Ladder;
import de.sommercom.collider.model.NullPlayer;
import de.sommercom.collider.model.Platform;
import de.sommercom.collider.model.Player;

public class LevelFactory {
	private static final float PLAYER_RADIUS = 4.0f;

	public static final String[] LEVELS = { "Level 1", "Two Balls",
			"Platform Test", "Ladder Test(experimental)", "Physics Benchmark" };

	public void loadLevel(GameWorld world, DimensionF abstractViewport, int level) {
		world.clearAll();

		switch (level) {
		case 1:
			world.add(new Ball(world, 23, 22, 2, 10, 0, 2));
			world.add(new Ball(world, 27, 22, 2, -10, 0, 2));
			createPlayer(world, abstractViewport.Width * 0.5f, PLAYER_RADIUS);
			break;
		case 2:
			world.add(new Platform(world,new Vec2(11.5f,16.0f),new Vec2(7.0f,0.5f)));
			world.add(new Ball(world, 23, 22, 2, 10, 0, 2));
			world.add(new Ball(world, 27, 22, 2, -10, 0, 2));
			createPlayer(world, abstractViewport.Width * 0.5f, PLAYER_RADIUS);
			break;
		case 3:
			world.add(new Platform(world,new Vec2(10.5f,16.0f),new Vec2(7.0f,0.5f)));
			world.add(new Ladder(world,new Vec2(21.5f,9.0f),new Vec2(4.0f,9.0f)));
			world.add(new Platform(world,new Vec2(32.5f,16.0f),new Vec2(7.0f,0.5f)));
			createPlayer(world, abstractViewport.Width * 0.5f, PLAYER_RADIUS);
			break;
		case 4:
			Random rand = new Random();
			int rows = 4,
			cols = 4;
			for (int x = 0; x < rows; x++) {
				for (int y = 0; y < cols; y++) {
					Vec2 direction = new Vec2(rand.nextFloat() * 2.0f - 1.0f,
							rand.nextFloat() * 2.0f - 1.0f);
					direction.normalize();
					direction.mul(rand.nextFloat() * 100.0f);
					world.add(new Ball(world, abstractViewport.Width
							/ (rows + 1) * (x + 1), abstractViewport.Height
							/ (cols + 1) * (y + 1), 2, direction.x, direction.y, 2));
				}
			}
			Player player = new NullPlayer();
			world.add(player);
			world.setPlayer(player);
			break;
		default:
			world.add(new Ball(world, 18, 22, 2, 10, 0, 2));
			createPlayer(world, abstractViewport.Width * 0.5f, PLAYER_RADIUS);
			break;
		}
	}

	private void createPlayer(GameWorld world, float x, float y) {
		ConcretePlayer player = new ConcretePlayer(world, x, y, PLAYER_RADIUS);
		world.add(player);
		world.setPlayer(player);
	}
}
