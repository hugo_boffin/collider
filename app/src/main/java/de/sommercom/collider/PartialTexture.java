package de.sommercom.collider;

import de.sommercom.collider.model.Dimension;
import de.sommercom.collider.model.Position;

public class PartialTexture implements Texture {
//	private final Texture source;
	
	private final int handle;
	private final Dimension size;
	private final int[] partialCrop;
		
	public PartialTexture(Texture source, Position position, Dimension dimension) {
//		this.source = source;
		this.handle = source.getHandle();
		this.size = dimension;
        //this.fullCrop = new int[] {0, bitmapSize.Height, bitmapSize.Width, -bitmapSize.Height};
		this.partialCrop = new int[] {position.x, position.y+size.Height, +size.Width, -size.Height};
//		Log.d("",Arrays.toString(this.partialCrop));
	}

	@Override
	public int getHandle() {
		return handle;
	}

	@Override
	public Dimension getSize() {
		return size;
	}

	@Override
	public int[] getCrop() {
		return partialCrop;
	}

}
