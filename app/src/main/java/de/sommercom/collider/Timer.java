package de.sommercom.collider;

import java.util.Observable;

public class Timer extends Observable {

	private interface Counter {
		float getInversePrecision();

		long getCurrentTime();
	}

	private final Counter counter;
	private long frameRate = -1;
	private long frameCounter = 0;
	private long lastCurrentTime;
	private long actCurrentTime;
	private float elapsedTimeSeconds = 0.0f;
	private float totalElapsedTimeSeconds = 0.0f;

	public Timer(boolean higherPrecision) {
		if (higherPrecision) {
			counter = new Counter() {
				private static final float PRECISION = 1.0f / 1000000000.0f;

				@Override
				public float getInversePrecision() {
					return PRECISION;
				}

				@Override
				public long getCurrentTime() {
					return System.nanoTime();
				}
			};
		} else {
			counter = new Counter() {
				private static final float PRECISION = 1.0f / 1000.0f;

				@Override
				public float getInversePrecision() {
					return PRECISION;
				}

				@Override
				public long getCurrentTime() {
					return System.currentTimeMillis();
				}
			};
		}
	}

	public void step() {
		frameCounter++;

		actCurrentTime = counter.getCurrentTime();

		elapsedTimeSeconds = (actCurrentTime - lastCurrentTime)
				* counter.getInversePrecision();

		totalElapsedTimeSeconds += elapsedTimeSeconds;

		if (totalElapsedTimeSeconds >= 1.0f) {
			totalElapsedTimeSeconds -= 1.0f;
			frameRate = frameCounter;
			frameCounter = 0;

			super.setChanged();
			super.notifyObservers();
		}

		lastCurrentTime = counter.getCurrentTime();
	}

	public void stepNoFPS() {
		actCurrentTime = counter.getCurrentTime();

		elapsedTimeSeconds = (actCurrentTime - lastCurrentTime)
				* counter.getInversePrecision();

		totalElapsedTimeSeconds += elapsedTimeSeconds;

		lastCurrentTime = counter.getCurrentTime();
	}
	
	public void reset() {
		lastCurrentTime = counter.getCurrentTime();
		elapsedTimeSeconds = 0.0f;
	}

	/**
	 * Scaled in Seconds 1s = 1.0
	 * 
	 * @return delta time
	 */
	public float getDeltaTime() {
		return elapsedTimeSeconds;
	}

	public long getFrameRate() {
		return frameRate;
	}
}
