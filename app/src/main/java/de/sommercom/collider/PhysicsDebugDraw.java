package de.sommercom.collider;

import org.jbox2d.callbacks.DebugDraw;
import org.jbox2d.common.Color3f;
import org.jbox2d.common.OBBViewportTransform;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;

import android.util.Log;

public class PhysicsDebugDraw extends DebugDraw {
	public PhysicsDebugDraw() {
		super(new OBBViewportTransform());
		Log.d("DebugDraw","PhysicsDebugDraw");
	}

	@Override
	public void drawCircle(Vec2 arg0, float arg1, Color3f arg2) {
		Log.d("DebugDraw",arg0.toString() + " "+arg1 + " "+ arg2.toString());
	}

	@Override
	public void drawPoint(Vec2 arg0, float arg1, Color3f arg2) {
		Log.d("DebugDraw","drawPoint");
	}

	@Override
	public void drawSegment(Vec2 arg0, Vec2 arg1, Color3f arg2) {
		Log.d("DebugDraw","drawSegment");
	}

	@Override
	public void drawSolidCircle(Vec2 arg0, float arg1, Vec2 arg2, Color3f arg3) {
		Log.d("DebugDraw",arg0.toString() + " "+arg1 + " "+ arg2.toString() + " "+ arg3.toString());
	}

	@Override
	public void drawSolidPolygon(Vec2[] arg0, int arg1, Color3f arg2) {
		Log.d("DebugDraw","drawSolidPolygon");
	}

	@Override
	public void drawString(float arg0, float arg1, String arg2, Color3f arg3) {
		Log.d("DebugDraw","drawString");
	}

	@Override
	public void drawTransform(Transform arg0) {
		Log.d("DebugDraw","drawTransform");
	}
}
